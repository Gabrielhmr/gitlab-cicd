FROM node:latest
MAINTAINER Gabriel Rodrigues
COPY . /var/www
WORKDIR /var/www
RUN npm install
ENTRYPOINT npm start
EXPOSE 3000